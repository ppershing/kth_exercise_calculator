import collections

Token = collections.namedtuple("Token", ["typ", "content", "pos"])
TOK_NUMBER=1
TOK_PLUS=2
TOK_MINUS=3
TOK_LPAREN=4
TOK_RPAREN=5

def tokenize(sentence):
    state = None
    content = None
    content_pos = None
    for i, c in enumerate(sentence):
        if c in ['-', '+', '(', ')']:
            if state:
                yield Token(state, content, content_pos)
            char_to_token = {
                '-': TOK_MINUS,
                '+': TOK_PLUS,
                '(': TOK_LPAREN,
                ')': TOK_RPAREN,
            }
            yield Token(char_to_token[c], c, i)
            state=None
        elif c == ' ':
            if state:
                yield Token(state, content, content_pos)
            state = None
        elif c in "0123456789.":
            if state != TOK_NUMBER:
                if state:
                    yield Token(state, content, content_pos)
                state = TOK_NUMBER
                content = ""
                content_pos = i
            content += c
        else:
            raise RuntimeError("Bad character '%s' at %d" % (c, i))
    if state:
        yield Token(state, content, content_pos)

#######################

class AstNode(object):
    pass

class NumberNode(AstNode):
    def __init__(self, value):
        self.value = value

    def visit(self, visitor):
        return visitor.visit_number(self)

    def __eq__(self, other):
        return isinstance(other, NumberNode) and self.value == other.value

class BinaryOp(AstNode):
    def __init__(self, op, left, right):
        self.op = op
        self.left = left
        self.right = right

    def visit(self, visitor):
        return visitor.visit_binary_op(self)

    def __eq__(self, other):
        return (
            isinstance(other, BinaryOp) and
            self.op == other.op and
            self.left == other.left and
            self.right == other.right
        )

def _parse_until_rparen(token_iterator):
    """Warning: consumes also TOK_RPAREN!
    """
    lhs = next(token_iterator)
    if lhs.typ == TOK_LPAREN:
        lhs = _parse_until_rparen(token_iterator)
    elif lhs.typ != TOK_NUMBER:
        raise RuntimeError(
            "Pos %d: expected expression, got '%s'" %
            (lhs.pos, lhs.content)
        )
    else:
        lhs = NumberNode(float(lhs.content))

    while True:
        op = next(token_iterator)
        if op.typ == TOK_RPAREN:
            return lhs
        elif op.typ not in [TOK_PLUS, TOK_MINUS]:
            raise RuntimeError(
                "Pos %d: expected binary operator, got %s" %
                (op.pos, op.content)
            )
        rhs = next(token_iterator)
        if rhs.typ == TOK_LPAREN:
            rhs = _parse_until_rparen(token_iterator)
        elif rhs.typ != TOK_NUMBER:
            raise RuntimeError(
                "Pos %d: expected expression, got '%s'" % 
                (rhs.pos, rhs.content)
            )
        else:
            rhs = NumberNode(float(rhs.content))

        lhs = BinaryOp(op.content, lhs, rhs)


def make_ast(tokens):
    tokens = list(tokens)
    tokens.append(Token(TOK_RPAREN, "END OF LINE", -1))
    it = iter(tokens)
    result = _parse_until_rparen(it)
    try:
        token = next(it)
    except StopIteration:
        pass
    else:
        raise RuntimeError("Stray content: %s" % token)
    return result

#######################

class EvalVisitor:
    def visit_number(self, node):
        return node.value

    def visit_binary_op(self, node):
        if node.op == '+':
            return node.left.visit(self) + node.right.visit(self)
        elif node.op == '-':
            return node.left.visit(self) - node.right.visit(self)
        else:
            raise NotImplementedError()

class PprintVisitor:
    def visit_number(self, node):
        return str(node.value)

    def visit_binary_op(self, node):
        return "(" + node.left.visit(self) + " " + node.op + " " + node.right.visit(self) + ")"


if __name__ == "__main__":
    line = raw_input()
    ast_root = make_ast(tokenize(line))
    printer = PprintVisitor()
    print "Parsed as:", ast_root.visit(printer)
    evaluator = EvalVisitor()
    print "Result:", ast_root.visit(evaluator)

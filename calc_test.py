import unittest
from calc import *

class Test(unittest.TestCase):
    ### TOKENIZING
    def test_tokenize_simple(self):
        self.assertEquals(
            list(tokenize("42+47")),
            [Token(TOK_NUMBER, "42", 0),
             Token(TOK_PLUS, "+", 2),
             Token(TOK_NUMBER, "47", 3),
            ]
        )
    
    def test_tokenize_float(self):
        self.assertEquals(
            list(tokenize("1.47")),
            [Token(TOK_NUMBER, "1.47", 0),]
        )
    
    
    def test_tokenize_complex(self):
        self.assertEquals(
            list(tokenize("((1+1)-2+3)")),
            [
                (TOK_LPAREN,   "(", 0),
                (TOK_LPAREN,   "(", 1),
                (TOK_NUMBER,   "1", 2),
                (TOK_PLUS,     "+", 3),
                (TOK_NUMBER,   "1", 4),
                (TOK_RPAREN,   ")", 5),
                (TOK_MINUS,    "-", 6),
                (TOK_NUMBER,   "2", 7),
                (TOK_PLUS,     "+", 8),
                (TOK_NUMBER,   "3", 9),
                (TOK_RPAREN,   ")", 10),
            ]
        )

    ########## AST
    def test_ast_simple(self):
        self.assertEquals(
            make_ast([
                Token(TOK_NUMBER, "42", None),
                Token(TOK_PLUS,   "+", None),
                Token(TOK_NUMBER, "47", None),
            ]),
            BinaryOp("+", NumberNode(42), NumberNode(47))
        )
    
    def test_ast_subsume_parens(self):
        # "((42)+((47)))" --> 42+47
        self.assertEquals(
            make_ast([
                Token(TOK_LPAREN, "(", None),
                Token(TOK_LPAREN, "(", None),
                Token(TOK_NUMBER, "42", None),
                Token(TOK_RPAREN, ")", None),
                Token(TOK_PLUS,   "+", None),
                Token(TOK_LPAREN, "(", None),
                Token(TOK_LPAREN, "(", None),
                Token(TOK_NUMBER, "47", None),
                Token(TOK_RPAREN, ")", None),
                Token(TOK_RPAREN, ")", None),
                Token(TOK_RPAREN, ")", None),
            ]),
            BinaryOp("+", NumberNode(42), NumberNode(47))
        )
    
    def test_ast_chain(self):
        # 1+2-3+4-5
        self.assertEquals(
            make_ast([
                Token(TOK_NUMBER, "1", None),
                Token(TOK_PLUS,   "+", None),
                Token(TOK_NUMBER, "2", None),
                Token(TOK_MINUS,  "-", None),
                Token(TOK_NUMBER, "3", None),
                Token(TOK_PLUS,   "+", None),
                Token(TOK_NUMBER, "4", None),
                Token(TOK_MINUS,  "-", None),
                Token(TOK_NUMBER, "5", None),
            ]),
            BinaryOp('-',
                BinaryOp('+',
                    BinaryOp('-',
                        BinaryOp('+',
                            NumberNode(1),
                            NumberNode(2)
                        ),
                        NumberNode(3)
                    ),
                    NumberNode(4)
                ),
                NumberNode(5)
            )
        )
    
    def test_ast_paren_priority(self):
        self.assertEquals(
            make_ast(tokenize("1+(2-(3+(4-5)))")),
            BinaryOp('+',
                NumberNode(1),
                BinaryOp('-',
                    NumberNode(2),
                    BinaryOp('+',
                        NumberNode(3),
                        BinaryOp('-',
                            NumberNode(4),
                            NumberNode(5)
                        ),
                    ),
                ),
            )
        )

    ## PRINTER
    def test_printer_simple(self):
        self.assertEquals(
            BinaryOp('+',
                NumberNode(1),
                NumberNode(2)
            ).visit(PprintVisitor()),
            '(1 + 2)'
        )
    
    def test_printer_nested(self):
        self.assertEquals(
            BinaryOp('+',
                NumberNode(1),
                BinaryOp('-',
                    NumberNode(2),
                    NumberNode(3),
                )
            ).visit(PprintVisitor()),
            '(1 + (2 - 3))'
        )

if __name__ == "__main__":
    unittest.main()
